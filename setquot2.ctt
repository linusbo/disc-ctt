
module setquot2 where

import sigma
import pi
import univalence

opaque lemPropF
opaque setPi
opaque subst

-- Start of source code taken from setquot.ctt.

subtypeEquality (A : U) (B : A -> U) (pB : (x : A) -> prop (B x))
                (s t : (x : A) * B x) : Path A s.1 t.1 -> Path (Sigma A B) s t
  = trans (Path A s.1 t.1) (Path (Sigma A B) s t) rem
  where
  rem : Path U (Path A s.1 t.1) (Path (Sigma A B) s t)
    = <i> lemSigProp A B pB s t @ -i

opaque subtypeEquality

-- (* Propositions *)

hProp : U
  = (X : U) * prop X

ishinh_UU (X : U) : U
  = (P : hProp) -> ((X -> P.1) -> P.1)

propishinh (X : U) : prop (ishinh_UU X)
  = propPi hProp (\(P : hProp) -> ((X -> P.1) -> P.1)) rem1
  where
  rem1 (P : hProp) : prop ((X -> P.1) -> P.1)
    = propPi (X -> P.1) (\(_ : X -> P.1) -> P.1) (\(f : X -> P.1) -> P.2)

ishinh (X : U) : hProp
  = (ishinh_UU X, propishinh X)

hinhpr (X : U) : X -> (ishinh X).1
  = \(x : X) (P : hProp) (f : X -> P.1) -> f x

hinhuniv (X : U) (P : hProp) (f : X -> P.1) (inhX : (ishinh X).1) : P.1
  = inhX P f

-- Direct proof that logical equivalence is equiv for props
isEquivprop (A B : U) (f : A -> B) (g : B -> A) (pA : prop A) (pB : prop B)
  : isEquiv A B f
  = rem
  where
  rem (y : B) : isContr (fiber A B f y) = (s,t)
    where
    s : fiber A B f y
      = (g y,pB y (f (g y)))
    t (w : fiber A B f y) : Path ((x :  A) * Path B y (f x)) s w
      = subtypeEquality A (\(x : A) -> Path B y (f x)) pb s w r1
      where
      pb (x : A) : (a b : Path B y (f x)) -> Path (Path B y (f x)) a b
        = propSet B pB y (f x)
      r1 : Path A s.1 w.1
        = pA s.1 w.1

equivhProp (P P' : hProp) (f : P.1 -> P'.1) (g : P'.1 -> P.1) : equiv P.1 P'.1
  = (f, isEquivprop P.1 P'.1 f g P.2 P'.2)

-- Direct proof of uahp
uahp (P P' : hProp) (f : P.1 -> P'.1) (g : P'.1 -> P.1) : Path hProp P P'
  = subtypeEquality U prop propIsProp P P' rem
  where
  rem : Path U P.1 P'.1
    = isoPath P.1 P'.1 f g s t
    where
    s (y : P'.1) : Path P'.1 (f (g y)) y
      = P'.2 (f (g y)) y
    t (x : P.1) : Path P.1 (g (f x)) x
      = P.2 (g (f x)) x

opaque uahp

-- A short proof that hProp form a set using univalence: (this is not needed!)

propequiv (X Y : U) (H : prop Y) (f g : equiv X Y) : Path (equiv X Y) f g
  = equivLemma X Y f g (<i> \(x : X) -> H (f.1 x) (g.1 x) @ i)

propidU (X Y : U) : Path U X Y -> prop Y -> prop X
  = substInv U prop X Y

sethProp (P P' : hProp) : prop (Path hProp P P')
  = propidU (Path hProp P P') (equiv P.1 P'.1) rem (propequiv P.1 P'.1 P'.2)
  where
  rem1 : Path U (Path hProp P P') (Path U P.1 P'.1)
    = lemSigProp U prop propIsProp P P'
  rem2 : Path U (Path U P.1 P'.1) (equiv P.1 P'.1)
    = corrUniv P.1 P'.1
  rem : Path U (Path hProp P P') (equiv P.1 P'.1)
    = compPath U (Path hProp P P') (Path U P.1 P'.1) (equiv P.1 P'.1) rem1 rem2

opaque sethProp

-- (* Sets *)

hsubtypes (X : U) : U
  = X -> hProp

carrier (X : U) (A : hsubtypes X) : U
  = Sigma X (\(x : X) -> (A x).1)

sethsubtypes (X : U) : set (hsubtypes X)
  = setPi X (\(_ : X) -> hProp) (\(_ : X) -> sethProp)

-- Definition hrel (X : UU) := X -> X -> hProp.
hrel (X : U) : U
  = X -> X -> hProp

iseqclass (X : U) (R : hrel X) (A : hsubtypes X) : U
  = and (and (ishinh (carrier X A)).1
          ((x1 x2 : X) -> (R x1 x2).1 -> (A x1).1 -> (A x2).1))
          ((x1 x2 : X) -> (A x1).1 -> (A x2).1 -> (R x1 x2).1)

eqax0 (X : U) (R : hrel X) (A : hsubtypes X) (eqc : iseqclass X R A)
  : (ishinh (carrier X A)).1
  = eqc.1.1
eqax1 (X : U) (R : hrel X) (A : hsubtypes X) (eqc : iseqclass X R A)
  : (x1 x2 : X) -> (R x1 x2).1 -> (A x1).1 -> (A x2).1
  = eqc.1.2
eqax2 (X : U) (R : hrel X) (A : hsubtypes X) (eqc : iseqclass X R A)
  : (x1 x2 : X) -> (A x1).1 -> (A x2).1 -> (R x1 x2).1
  = eqc.2

propiseqclass (X : U) (R : hrel X) (A : hsubtypes X) : prop (iseqclass X R A)
  = propAnd (and (ishinh (carrier X A)).1
                 ((x1 x2 : X) -> (R x1 x2).1 -> (A x1).1 -> (A x2).1))
            ((x1 x2 : X) -> (A x1).1 -> (A x2).1 -> (R x1 x2).1)
            (propAnd (ishinh (carrier X A)).1
              ((x1 x2 : X) -> (R x1 x2).1 -> (A x1).1 -> (A x2).1) p1 p2)
            p3
  where
  p1 : prop (ishinh (carrier X A)).1
    = propishinh (carrier X A)

  -- This proof is quite cool, but it looks ugly...
  p2 (f g : (x1 x2 : X) -> (R x1 x2).1 -> (A x1).1 -> (A x2).1)
    : Path ((x1 x2 : X) -> (R x1 x2).1 -> (A x1).1 -> (A x2).1) f g
    = <i> \(x1 x2 : X) (h1 : (R x1 x2).1) (h2 : (A x1).1) ->
         (A x2).2 (f x1 x2 h1 h2) (g x1 x2 h1 h2) @ i

  p3 (f g : (x1 x2 : X) -> (A x1).1 -> (A x2).1 -> (R x1 x2).1)
    : Path ((x1 x2 : X) -> (A x1).1 -> (A x2).1 -> (R x1 x2).1) f g
    = <i> \(x1 x2 : X) (h1 : (A x1).1) (h2 : (A x2).1) ->
        (R x1 x2).2 (f x1 x2 h1 h2) (g x1 x2 h1 h2) @ i

opaque propiseqclass

isrefl (X : U) (R : hrel X) : U
  = (x : X) -> (R x x).1
issymm (X : U) (R : hrel X) : U
  = (x1 x2 : X) -> (R x1 x2).1 -> (R x2 x1).1
istrans (X : U) (R : hrel X) : U
  = (x1 x2 x3 : X) -> (R x1 x2).1 -> (R x2 x3).1 -> (R x1 x3).1

ispreorder (X : U) (R : hrel X) : U
  = and (istrans X R) (isrefl X R)

iseqrel (X : U) (R : hrel X) : U
  = and (ispreorder X R) (issymm X R)

eqrel (X : U) : U
  = (R : hrel X) * (iseqrel X R)

eqrelrefl (X : U) (R : eqrel X) : isrefl X R.1
  = R.2.1.2
eqrelsymm (X : U) (R : eqrel X) : issymm X R.1
  = R.2.2
eqreltrans (X : U) (R : eqrel X) : istrans X R.1
  = R.2.1.1

setquot (X : U) (R : hrel X) : U
  = (A : hsubtypes X) * (iseqclass X R A)

setquotpr (X : U) (R : eqrel X) (X0 : X) : setquot X R.1
  = (A, ((p1,p2),p3))
  where
  rax : isrefl X R.1
    = eqrelrefl X R
  sax : issymm X R.1
    = eqrelsymm X R
  tax : istrans X R.1
    = eqreltrans X R
  A : hsubtypes X
    = \(x : X) -> R.1 X0 x
  p1 : (ishinh (carrier X A)).1
    = hinhpr (carrier X A) (X0,rax X0)
  p2 (x1 x2 : X) (X1 : (R.1 x1 x2).1) (X2 : (A x1).1) : (A x2).1
    = tax X0 x1 x2 X2 X1
  p3 (x1 x2 : X) (X1 : (A x1).1) (X2 : (A x2).1) : (R.1 x1 x2).1
    = tax x1 X0 x2 (sax X0 x1 X1) X2

setquotl0 (X : U) (R : eqrel X) (c : setquot X R.1) (x : carrier X c.1)
  : Path (setquot X R.1) (setquotpr X R x.1) c
  = subtypeEquality (hsubtypes X) (iseqclass X R.1) p (setquotpr X R x.1) c rem
  where
  p (A : hsubtypes X) : prop (iseqclass X R.1 A)
    = propiseqclass X R.1 A
  rem : Path (hsubtypes X) (setquotpr X R x.1).1 c.1
    = <i> \(x : X) -> (rem' x) @ i -- inlined use of funext
    where
    rem' (a : X) : Path hProp ((setquotpr X R x.1).1 a) (c.1 a)
      = uahp ((setquotpr X R x.1).1 a) (c.1 a) l2r r2l   -- This is where uahp appears
      where
      l2r (r : ((setquotpr X R x.1).1 a).1) : (c.1 a).1
        = eqax1 X R.1 c.1 c.2 x.1 a r x.2
      r2l : (c.1 a).1 -> ((setquotpr X R x.1).1 a).1
        = eqax2 X R.1 c.1 c.2 x.1 a x.2

setquotprop (X : U) (R : eqrel X) (P : setquot X R.1 -> hProp)
  (ps : (x : X) -> (P (setquotpr X R x)).1) (c : setquot X R.1) : (P c).1
  = hinhuniv (carrier X c.1) (P c) f rem
  where
  f (x : carrier X c.1) : (P c).1
    = let
      e : Path (setquot X R.1) (setquotpr X R x.1) c
        = setquotl0 X R c x
    in subst (setquot X R.1) (\(w : setquot X R.1) -> (P w).1)
         (setquotpr X R x.1) c e (ps x.1)
  rem : (ishinh (carrier X c.1)).1
    = eqax0 X R.1 c.1 c.2


setquotprop2 (X : U) (R : eqrel X) (P : setquot X R.1 -> setquot X R.1 -> hProp)
  (ps : (x x' : X) -> (P (setquotpr X R x) (setquotpr X R x')).1) (c c' : setquot X R.1)
  : (P c c').1
  = setquotprop X R (\ (c0' : setquot X R.1) -> P c c0')
      (\ (x : X) -> setquotprop X R (\ (c0 : setquot X R.1) ->
        P c0 (setquotpr X R x)) (\ (x0 : X) -> ps x0 x) c) c'


setquotprop3 (X : U) (R : eqrel X)
    (P : setquot X R.1 -> setquot X R.1 -> setquot X R.1 -> hProp)
    (ps : (x0 x1 x2 : X) -> (P (setquotpr X R x0) (setquotpr X R x1) (setquotpr X R x2)).1)
    (q0 q1 q2 : setquot X R.1) : (P q0 q1 q2).1
  = let
    Q : U
      = setquot X R.1
    qpr (x : X) : Q
      = setquotpr X R x
    P' (q2' : Q) : hProp
      = P q0 q1 q2'
    ps' (x2' : X) : (P q0 q1 (qpr x2')).1
      = let
        P'' (q1' : Q) : hProp
          = P q0 q1' (qpr x2')
        ps'' (x1' : X) : (P q0 (qpr x1') (qpr x2')).1
          = let
            P''' (q0' : Q) : hProp
              = P q0' (qpr x1') (qpr x2')
            ps''' (x0' : X) : (P (qpr x0') (qpr x1') (qpr x2')).1
              = ps x0' x1' x2'
          in setquotprop X R P''' ps''' q0
      in setquotprop X R P'' ps'' q1
  in setquotprop X R P' ps' q2


setsetquot (X : U) (R : hrel X) : set (setquot X R)
  = setSig (hsubtypes X) (\(A : hsubtypes X) -> iseqclass X R A) sA sB
  where
  sA : set (hsubtypes X)
    = sethsubtypes X
  sB (x : hsubtypes X) : set (iseqclass X R x)
    = propSet (iseqclass X R x) (propiseqclass X R x)


iscompsetquotpr (X : U) (R : eqrel X) (x x' : X) (a : (R.1 x x').1)
  : Path (setquot X R.1) (setquotpr X R x) (setquotpr X R x')
  = subtypeEquality (hsubtypes X) (iseqclass X R.1) rem1 (setquotpr X R x)
      (setquotpr X R x') rem2
  where
  rem1 (x : hsubtypes X) : prop (iseqclass X R.1 x)
    = propiseqclass X R.1 x
  rem2 : Path (hsubtypes X) (setquotpr X R x).1 (setquotpr X R x').1
    = <i> \(x0 : X) -> rem x0 @ i
    where
    rem (x0 : X) : Path hProp (R.1 x x0) (R.1 x' x0)
      = uahp (R.1 x x0) (R.1 x' x0) f g
      where
      f (r0 : (R.1 x x0).1) : (R.1 x' x0).1
        = eqreltrans X R x' x x0 (eqrelsymm X R x x' a) r0
      g (r0 : (R.1 x' x0).1) : (R.1 x x0).1
        = eqreltrans X R x x' x0 a r0

-- End of source code take from setquot.ctt

funresprel (A B : U) (f : A -> B) (R : hrel A) : U
  = (a a' : A) (r : (R a a').1) -> Path B (f a) (f a')


funresprel2 (A B C : U) (f : A -> B -> C) (R0 : hrel A) (R1 : hrel B) : U
  = (a a' : A) (b b' : B) -> (R0 a a').1 -> (R1 b b').1 -> Path C (f a b) (f a' b')

hProppair (X Y : hProp) : hProp
  = (and X.1 Y.1, propAnd X.1 Y.1 X.2 Y.2)

hrelpair (A B : U) (R0 : hrel A) (R1 : hrel B) (x y : and A B) : hProp
  = hProppair (R0 x.1 y.1) (R1 x.2 y.2)

iseqrelpair (A B : U) (R0 : hrel A) (R1 : hrel B) (E0 : iseqrel A R0)
    (E1 : iseqrel B R1) : iseqrel (and A B) (hrelpair A B R0 R1)
  = let
    T : U
      = and A B
    R : hrel T
      = hrelpair A B R0 R1
    rax : isrefl T R
      = \ (t0 : T) -> (E0.1.2 t0.1, E1.1.2 t0.2)
    sax : issymm T R
      = \ (t0 t1 : T) (e : (R t0 t1).1) -> (E0.2 t0.1 t1.1 e.1, E1.2 t0.2 t1.2 e.2)
    tax : istrans T R
      = \ (t0 t1 t2 : T) (e0 : (R t0 t1).1) (e1 : (R t1 t2).1) ->
            (E0.1.1 t0.1 t1.1 t2.1 e0.1 e1.1, E1.1.1 t0.2 t1.2 t2.2 e0.2 e1.2)
  in ((tax, rax), sax)

eqrelpair (A B : U) (R0 : eqrel A) (R1 : eqrel B) : eqrel (and A B)
  = (hrelpair A B R0.1 R1.1, iseqrelpair A B R0.1 R1.1 R0.2 R1.2)

hsubtypespair (A B : U) (H0 : hsubtypes A) (H1 : hsubtypes B) (x : and A B) : hProp
  = hProppair (H0 x.1) (H1 x.2)

iseqclasspair (A B : U) (R0 : hrel A) (R1 : hrel B) (H0 : hsubtypes A)
    (H1 : hsubtypes B) (E0 : iseqclass A R0 H0) (E1 : iseqclass B R1 H1)
  : iseqclass (and A B) (hrelpair A B R0 R1) (hsubtypespair A B H0 H1)
  = let
    T : U
      = and A B
    R : hrel T
      = hrelpair A B R0 R1
    H : hsubtypes T
      = hsubtypespair A B H0 H1
    a (P : hProp) (f : carrier T H -> P.1) : P.1
      = let
        g (x0 : carrier A H0) : P.1
          = let
            h (x1 : carrier B H1) : P.1
              = f ((x0.1, x1.1), (x0.2, x1.2))
          in E1.1.1 P h
      in E0.1.1 P g
    b (x0 x1 : T) (r : (R x0 x1).1) (h0 : (H x0).1) : (H x1).1
      = (E0.1.2 x0.1 x1.1 r.1 h0.1, E1.1.2 x0.2 x1.2 r.2 h0.2)
    c (x0 x1 : T) (h0 : (H x0).1) (h1 : (H x1).1) : (R x0 x1).1
      = (E0.2 x0.1 x1.1 h0.1 h1.1, E1.2 x0.2 x1.2 h0.2 h1.2)
  in ((a, b), c)

setquotpair (A B : U) (R0 : hrel A) (R1 : hrel B) (q0 : setquot A R0)
    (q1 : setquot B R1)
  : setquot (and A B) (hrelpair A B R0 R1)
  = let
    T : U
      = and A B
    R : hrel T
      = hrelpair A B R0 R1
    H : hsubtypes T
      = hsubtypespair A B q0.1 q1.1
    E : iseqclass T R H
      = iseqclasspair A B R0 R1 q0.1 q1.1 q0.2 q1.2
  in (H, E)

setquotunivprop' (A B : U) (sB : set B) (R : hrel A) (f : A -> B)
    (frr : funresprel A B f R) (c : setquot A R)
  : isContr ((y : B) * ((x : carrier A c.1) -> Path B y (f x.1)))
  = let
    T : U = (y : B) * ((x : carrier A c.1) -> Path B y (f x.1))
    pT (a b : T) : Path T a b
      = let
        h (x : carrier A c.1) : Path B a.1 b.1 = <i> comp (<j> B) (a.2 x @ i)
          [ (i = 0) -> <j> a.1
          , (i = 1) -> <j> b.2 x @ -j ]
        p0 : Path B a.1 b.1
          = c.2.1.1 (Path B a.1 b.1, sB a.1 b.1) h
        p1 : PathP (<i> (x : carrier A c.1) -> Path B (p0 @ i) (f x.1)) a.2 b.2
          = let
            P (b : B) : U
              = (x : carrier A c.1) -> Path B b (f x.1)
            pP (b : B) (s t : (P b)) : Path (P b) s t
              = <i>  \ (x : carrier A c.1) -> (sB b (f x.1) (s x) (t x)) @ i
          in lemPropF B P pP a.1 b.1 p0 a.2 b.2
      in <i> (p0 @ i, p1 @ i)
    h (x : carrier A c.1) : T
      = (f x.1, \ (x' : carrier A c.1) -> frr x.1 x'.1 (c.2.2 x.1 x'.1 x.2 x'.2))
    y : T
      = c.2.1.1 (T, pT) h
  in (y, pT y)

setquotunivprop (A B : U) (sB : set B) (R : eqrel A) (f : A -> B)
    (frr : funresprel A B f R.1)
  : isContr ( (g : setquot A R.1 -> B)
            * (Path (A -> B) (\ (a : A) -> g (setquotpr A R a)) f))
  = let
    G : U
      = setquot A R.1 -> B
    I (g : G) : U
      = Path (A -> B) (\ (a : A) -> g (setquotpr A R a)) f
    pI (g : G) : prop (I g)
      = setPi A (\ (a : A) -> B) (\ (a : A) -> sB) (\ (a : A) -> g (setquotpr A R a)) f
    g : G
      = \ (x : setquot A R.1) -> (setquotunivprop' A B sB R.1 f frr x).1.1
    i : I g
      = <i> \ (a : A) ->
        (setquotunivprop' A B sB R.1 f frr (setquotpr A R a)).1.2 (a, R.2.1.2 a) @ i
    eq (h : (g : G) * I g) : Path ((g : G) * I g) (g, i) h
      = let
        p0 : Path G g h.1
          = <j> \ (x : setquot A R.1) -> let
            P (y : setquot A R.1) : hProp
              = (Path B (g y) (h.1 y), sB (g y) (h.1 y))
            ps (a : A) : (P (setquotpr A R a)).1
              = <k> comp (<_> B) ((i @ k) a)
                [ (k = 0) -> <_> g (setquotpr A R a)
                , (k = 1) -> <l> (h.2 @ -l) a ]
          in setquotprop A R P ps x @ j
        p1 : PathP (<i> I (p0 @ i)) i h.2
          = lemPropF G I pI g h.1 p0 i h.2
      in <i> (p0 @ i, p1 @ i)
  in ((g, i), eq)

setquotmap (A B : U) (sB : set B) (R : hrel A) (f : A -> B)
    (frr : funresprel A B f R) (c : setquot A R) : B
  = (setquotunivprop' A B sB R f frr c).1.1

setquotmapeq (A B : U) (sB : set B) (R : eqrel A) (f : A -> B)
    (frr : funresprel A B f R.1) (x : A)
  : Path B (setquotmap A B sB R.1 f frr (setquotpr A R x)) (f x)
  = (setquotunivprop' A B sB R.1 f frr (setquotpr A R x)).1.2 (x, R.2.1.2 x)

setquotmap2 (A B C : U) (sC : set C) (R0 : hrel A) (R1 : hrel B)
    (f : A -> B -> C) (frr : funresprel2 A B C f R0 R1) (c0 : setquot A R0)
    (c1 : setquot B R1) : C
  = let
    T : U = and A B
    f' (t : T) : C
      = f t.1 t.2
    R' : hrel T
      = hrelpair A B R0 R1
    frr' (s t : T) (r : (R' s t).1) : Path C (f' s) (f' t)
      = frr s.1 t.1 s.2 t.2 r.1 r.2
    c' : setquot T R'
      = setquotpair A B R0 R1 c0 c1
  in setquotmap T C sC R' f' frr' c'

setquotmapeq2 (A B C : U) (sC : set C) (R0 : eqrel A) (R1 : eqrel B)
    (f : A -> B -> C) (frr : funresprel2 A B C f R0.1 R1.1) (x0: A) (x1 : B)
  : Path C (setquotmap2 A B C sC R0.1 R1.1 f frr (setquotpr A R0 x0) (setquotpr B R1 x1)) (f x0 x1)
  = let
    T : U = and A B
    f' (t : T) : C
      = f t.1 t.2
    R : eqrel T
      = eqrelpair A B R0 R1
    frr' (s t : T) (r : (R.1 s t).1) : Path C (f' s) (f' t)
      = frr s.1 t.1 s.2 t.2 r.1 r.2
    c : setquot T R.1
      = setquotpair A B R0.1 R1.1 (setquotpr A R0 x0) (setquotpr B R1 x1)
  in (setquotunivprop' T C sC R.1 f' frr' c).1.2 ((x0, x1), R.2.1.2 (x0, x1))

setquot_inh (X : U) (R : eqrel X) (c : setquot X R.1) (P : hProp)
    (f : (x : X) (p : Path (setquot X R.1) (setquotpr X R x) c) -> P.1)
  : P.1
  = c.2.1.1 P (\ (x : carrier X c.1) -> f x.1 (setquotl0 X R c x))

setquot_rel (X : U) (R : eqrel X) (x x' : X)
    (p : Path (setquot X R.1) (setquotpr X R x) (setquotpr X R x'))
  : (R.1 x x').1
  = transport (<i> ((p @ -i).1 x').1) (R.2.1.2 x') 

setquot_mk (X : U) (R : hrel X) (A : hsubtypes X) (e : iseqclass X R A)
  : setquot X R
  = (A, e)




