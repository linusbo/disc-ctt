# README #

This repository will eventually contain some discrete math formalised in cubicaltt.

Files
=====

The contents of the following files is fairly stable:

setquot.ctt
-----------

Contains a construction of typequotients, where
  - subtypes are defined,
  - relations, equivalence relations are defined
  - equivalence classes are defined
  - setquotients are defined
  - the universal property of setquotients is proved
  - a map over setquotients is defined
  
subset.ctt
----------

Defines two alternative notions if subsets, and proves them equal.

injective.ctt
-------------

Defines two alternative notions of injectivity, and proves the equal.

graph.ctt
---------

Provides a definition of a graph, and proves some properties

